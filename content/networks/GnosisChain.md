**[←](/projects/)**
### Gnosis Chain

<center>

[<img src="/images/Gnosis_logo.png" width="200" height="200" />](/images/Gnosis_logo.png)

</center>


___
___
# What is it about?


The [Gnosis Chain](https://www.xdaichain.com/) is an Ethereum sidechain designed for fast and inexpensive transactions. A community-driven project that allows seamless migration of data, applications, and assets between the two networks in a secure way.

---
# Why we like it?


* Because it embraces a wide range of applications with low and stable fees. 
* Because it supports a decentralized, earth-friendly architecture with POSDAO consensus. 
* Because it has an ambitious technical roadmap that accelerates Ethereum and shares the same values, advocating for composability, permissionless innovation and transparency.
