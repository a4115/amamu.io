**[←](/projects/)**
### SSV Network

<center>

[<img src="/images/ssv.png" width="200" height="200" />](/images/ssv.png)

</center>



---
# What is it about?

[Secret Shared Validators (SSV)](https://ssv.network/) is the first secure and robust way to split a validator key for ETH staking between non-trusting nodes or operators. A unique protocol that enables the distributed control and operation of an Ethereum validator.

---
# Why we like it?

* Because it’s open to everyone and it's fault-tolerant.
* Because it allows you to optimize the performance of your validator while promoting decentralization across the entire network.
* Because the validator key is divided in such a way that no operator can take unilateral control of the network.

---
# How to stake ETH in SSV?

To run a SSV distributed validator, there are a two essential prerequisites:

1. **Minimum Stake Requirement**: You will need a minimum of 32 ETH for each staking validator you wish to run.

2. **SSV Tokens**: To operate validators on the SSV network, holding at least 1,5 SSV tokens per validator is recommended to cover yearly operational costs.


Distributed validators are managed within Clusters - the group of node operators that were selected to operate them.

To run a validator through the SSV network, a user must distribute their validator key to their selected cluster and register it to the network’s smart contract.

This can be done via the [web app interface](https://app.ssv.network/join)




<center>

---
## [Check our node operator](https://explorer.ssv.network/operators/181)
---

</center>
