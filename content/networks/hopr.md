**[←](/projects/)**
### HOPR Network

<center>

[<img src="/images/hopr.png" width="200" height="200" />](/images/hopr.png)

</center>

___
___
# What is it about?

The [HOPR Network](https://hoprnet.org/) is a decentralized and peer-to-peer network open to anyone. Their main goal is to provide metadata privacy by sending data packets through multiple nodes (or “hops”)  in the network. That is, since it is not done directly, the protocol allows sending messages from one point in a network to another without revealing from where, from whom or when that information was sent, or where it is going. 


# Why we like it?

* Because they show real commitment to network decentralization.
* Because the proof of relay consensus forces everyone participating in the network to play by the rules. Thanks to that, it can grow to an unlimited scale, because it’s not necessary to rely on finding trustworthy altruistic people to run it.
* Because it’s really open to anyone who wants to maintain the network by relaying data via a HOPR node, either on their computer or as a separate device plugged into their router.
