**[←](/projects/)**
### Namada
___
___
# What is it about?

[Namada](https://namada.net/) is a sovereign chain for asset-agnostic private transactions. It’s the first fractal instance launched as part of the Anoma network. The Anoma protocol is designed to facilitate the operation of networked fractal instances, which intercommunicate but can specialize in different tasks and serve different communities. The Namada protocol is focused exclusively on the use-case of private asset transfers (fungible or non-fungible ones). It allows to send them privately from Ethereum or any IBC chain, with a few second transaction latency and near-zero fees. 


# Why we like it?
- Because of its belief that privacy is a public good.
- Because of its outright opposition to an industry that turns people into a product.
- Because its commitment to be realized with user friendly interfaces.
