**[←](/projects/)**
### Fuse Network

<center>

[<img src="/images/fuse.png" width="200" height="200" />](/images/fuse.png)

</center>

___
___
# What is it about?

[Fuse Network](https://fuse.io) is an open-source decentralized blockchain-powered platform, whose goal is to enable genuine mass adoption of crypto payments and decentralized finance (DeFi). The platform puts communities in the hands of a technology that allows them to create their own economies and benefit from a set of reference products and tools.

---
# Why we like it?

* Because it’s a fast, decentralized and low cost alternative to existing digital payments. 
* Because there’s a great team working since 2019 to bring the opportunities offered by decentralized finance to anyone around the world, not just in the leading countries. 
* Because from the very beginning we were very excited about all the projects powered by the platform. All of them are good examples of what we think the new economy should be.

---
# Projects powered by Fuse

* [GoodDollar](https://www.gooddollar.org/), a digital universal basic income (UBI) project, which leverages DeFi’s strategies to create a free digital cash flow; 
* [Kolektivo](https://medium.com/fusenet/kolektivo-labs-launches-on-fuse-to-scale-blockchain-based-sustainable-development-in-cura%C3%A7ao-ac83d30294b), which develops regenerative economies and is now working on the island of Curaçao; 
* [Comunitaria](https://comunitaria.com/en/home/), which seeks to improve the solidarity economy in different cities in Spain; 
* [PeeplEat](https://itsaboutpeepl.com/), a project working to decentralize and disrupt the food delivery platforms like Deliveroo and UberEats. 
* [Wellbeing Protocol](https://www.thewellbeingprotocol.org/), an open source not-for-profit organization, that seeks to facilitate the creation of local communities focused on the wellbeing of their members. It is currently running the first pilot in Cannons Creek, New Zealand.
* [Flambu](https://www.flambu.com/), a mobile app that enables anyone to lend or rent anything on the platform. 


They all show that the Fuse Network makes real applications possible. That's what it's all about. We want to take advantage of all the tangible opportunities that the blockchain has to offer. We need practical solutions to make the world a better place.

---
# Are you ready for staking?


If you are already a Fuse Network enthusiast like us, you can get some Fuse Token and easily earn an interest on them, in a manner that is decentralized and non-custodial (which means that you will keep the control of your tokens all the time). 


Just get your rewards by delegating your tokens to a validator. This will give you some of the benefits of staking without the costs related to running your own node. 

---
# How to stake with Amamu?


We welcome all kinds of delegators, from institutional to individual holders. Each one of them will be considered the same.


To delegate your Fuse Tokens to Amamu you can follow the instructions in this [tutorial video](https://www.youtube.com/watch?v=askMo-eeFc0)


Here you also have [this guide](https://tutorials.fuse.io/tutorials/staking-tutorial), for metamask users, or [this one](https://tutorials.fuse.io/tutorials/staking-tutorial/staking-tutorial-using-ledger), if you're using Ledger.

