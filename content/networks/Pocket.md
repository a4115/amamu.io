**[←](/projects/)**
### ﻿POCKET NETWORK

<center>

[<img src="/images/pocket.png" width="200" height="200" />](/images/pocket.png)

</center>


___
___
# What is it about?


[Pocket Network](https://www.pokt.network/) is a decentralized web3 infrastructure. Its mission is to connect the different applications with the blockchains on which they are deployed. It does so through a network of RPC nodes that ensures that the interaction between the two layers is carried out in a completely decentralized manner. This, unfortunately, does not happen in other cases, in which the flow of information goes through a centralized intermediary that has nothing to do with the original values ​​of privacy and security with which blockchain technology was born.

---
# Why we like it?


- Because of its real commitment to privacy and real decentralization.
- Because it’s censorship-resistant and provides DApps and their users with unstoppable Web3 access. 
- Because it’s chain agnostic and everyday is supporting more and more networks.

