**[←](/projects/)**
### Penumbra Network
___
___
# What is it about?


[Penumbra](https://penumbra.zone/) is a fully private proof-of-stake network and decentralized exchange. This shielded, cross-chain network allows anyone to securely transact, stake, swap or marketmake without broadcasting their personal information to the world.


# Why we like it?


- Because the transactions are private by default and cannot be traced or decrypted without a user’s permission.
- Because stakeholders can privately delegate to a validator. Rewards accumulate in a delegation pool and are only earned when a delegator withdraws the delegation.
- Because of its shielded automated market maker design based on frequent batch auctions, which allows users to swap between any pair of assets without revealing their trading history.

