**[←](/projects/)**
### Stakewise

<center>

[<img src="/images/stakewise.jpeg" width="200" height="200" />](/images/stakewise.jpeg)

</center>

___
___
# What is it about?

[StakeWise](https://www.stakewise.io/) is a permissionless and decentralized staking protocol that offers a liquid staking solution for Ethereum (ETH). It offers a simple and secure way to stake ETH and, in return, receive a liquid staked-ETH derivative (osETH), which can be utilized in various ways within the DeFi ecosystem.

---
# Why we like it?

* Because anyone can join as a node operator without permission or collateral, including solo stakers.
* Because promote decentralization creating a node operator marketplace to help discerning and mission-driven users to choose their own staking providers.
* Because makes solo staking more appeling allowing them to access DeFi landscape.


---
# How to stake ETH in StakeWise?

Follow these steps to stake ETHs:

1. Choose a Vault in the [Vaults Marketplace](https://app.stakewise.io/vaults) section of the StakeWise platform.

2. Connect a compatible Ethereum wallet to interact with the Vault.

3. Choose the amount of ETH you wish to stake and follow the instructions to complete the transaction.

4. If you're interested, you can mint osETH, an overcollateralized liquid staking token against your staked ETH.

5. Once you have staked your ETH, you can track your rewards and returns directly from the StakeWise platform.


<center>

---
## [Check our Vault](https://app.stakewise.io/vault/mainnet/0xa1e229db735f7aa3882bfd22fa6b89734225a3d1)
---

</center>
