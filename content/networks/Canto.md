**[←](/projects/)**
<center>

[<img src="/images/canto.jpeg" width="200" height="200" />](/images/canto.jpeg)

</center>

___
___
# What is it about?

[Canto](https://canto.io/)  is a permissionless general-purpose blockchain running the Ethereum Virtual Machine (EVM). It was created to deliver on the promise of DeFi – that through a post-traditional financial movement, new systems will be made accessible, transparent, decentralized, and free. Created by a loosely organized collective of chain-native builders, Canto is a new commons powered by Free Public Infrastructure.

---
# Why we like it?

* Because it's designed to support free public infrastructure.
* Because its decentralized exchange protocol cannot be upgraded and will remain ungoverned.
* Because there are no venture backers.


---
# How to stake Canto?

Follow these steps to stake your CANTO:

1. Navigate to [v2.canto.io/staking](https://v2.canto.io/staking) and connect your wallet.

2. Click on the "all validators" tab to see a list of network validators

3. Click on the validator you would like to stake with

4. Enter the amount of CANTO you would like to stake and click "delegate".

5. Confirm the transaction in your wallet.


<center>

---
## [Check our node](https://v2.canto.io/staking)
---

</center>
