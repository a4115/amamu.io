**[←](/projects/)**
### Stader protocol

<center>

[<img src="/images/stader.png" width="200" height="200" />](/images/stader.png)

</center>
___
___
# What is it about?

[Stader Labs](https://www.staderlabs.com/), established in 2021, is a platform for earnings liquid staking rewards across crypto tokens such as Ethereum, polygon, BNB, & more.With support for six different networks, Stader Labs empowers users to deposit tokens into liquid staking contracts, enabling them to earn staking rewards while retaining the flexibility to utilize their crypto in various applications.


# Why we like it?

* Because is a non-custodial and permissionless staking platform.
* Because allows users to mint a token that represents their staked assets and grows in value as staking rewards accrue.
* Because is designed for decentralization adopting Distributed Validator Technology (DVT)
