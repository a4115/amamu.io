**[←](/projects/)**


<center>

[<img src="/images/Avalanche_logo_without_text.png" width="200" height="200" />](/images/Avalanche_logo_without_text.png)

</center>

---
---
# What is it about?


[Avalanche](https://www.avax.network/) is an open, programmable smart contracts platform for decentralized applications. The platform uses three separate subnets which allows it to improve scalability problems of other chains without compromising security. From the beginning it is designed to be universal and flexible and supports, in a value-neutral fashion, many blockchains to be built on top.

---
# Why we like it?

- Because it’s secure. Ensures stronger security guarantees well-above the 51% standard of other networks.
- Because of its commitment to multiple client implementations and no centralized control of any kind.
- Because of its speed and scalability. The network is able to permanently confirm transactions in under one second and capable of 4,500 transactions per second.

---
# How to stake AVAX?

Users can delegate their AVAX with ease through [Core Stake](https://core.app/stake/). By doing so, users can receive rewards on their delegated AVAX. The minimum amount required for staking is 25 AVAX.

You can find the steps on how delegate AVAX with Core StaKe in the following [guide](https://support.avax.network/en/articles/8117272-core-web-how-do-i-delegate-in-core-stake)

<center>

---
## [Check our node](https://avascan.info/staking/validator/NodeID-Ges52zCMZimxqri3ia1J32mUAKyUsvcpV)
---

</center>
