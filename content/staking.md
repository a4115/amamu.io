---
title: Stake with us
---
---


# Are you ready for staking?

Staking is essential to Proof-of-Stake blockchains and comes with many benefits for token stakers and the network:

* Stakers earn yield from token emissions and/or transaction fees.
* Staking helps to secure the network
* Staking from multiple participants helps decentralize the network
* Stakers can participate in governance, as many PoS networks allocate governance rights based on the amount of tokens staked.

Just get your rewards by delegating your tokens to a validator. This will give you some of the benefits of staking without the costs related to running your own node. 

---
# Why stake with Amamu?


* We truly believe in **decentralisation**, we operate minority clients, our infraestracture is distributed across underrepresented locations using a combination of cloud
* Our infrastructure is hosted on **top-tier data centers**
* We welcome **all kinds of delegators**, from institutional to individual holders. Each one of them will be considered the same.
* We **never had a slashing event** on any of the validators.
* We provide a **secure, realible and robust node**

---
# Which networks can you stake with us on?

---


<center>

## Ethereum, [Stakewise Vault](/networks/stakewise)
---

</center>


<center>

## Ethereum, [SSV operator](/networks/ssv)
---

</center>


<center>

## [Avalanche](/networks/avalanche)
---

</center>

<center>

## [Canto](/networks/canto)
---

</center>

<center>

## [Fuse](/networks/fuse)
---

</center>


