---
title: Projects
---
---

# **Networks where we run validators:**
___
___
___
<center>

[<img src="/images/Avalanche_logo_without_text.png" width="100" height="100" />](/images/Avalanche_logo_without_text.png)

## [Avalanche](/networks/avalanche/)


[<img src="/images/pocket.png" width="100" height="100" />](/images/pocket.png)

## [Pocket](/networks/pocket/)


[<img src="/images/fuse.png" width="100" height="100" />](/images/fuse.png)

## [Fuse](/networks/fuse/)


[<img src="/images/Gnosis_logo.png" width="100" height="100" />](/images/Gnosis_logo.png)

## [Gnosis](/networks/gnosischain/)


[<img src="/images/canto.jpeg" width="100" height="100" />](/images/canto.jpeg)

## [Canto](/networks/canto/)

[<img src="/images/ssv.png" width="100" height="100" />](/images/ssv.png)

## [SSV](/networks/ssv/)

[<img src="/images/stakewise.jpeg" width="100" height="100" />](/images/stakewise.jpeg)

## [Stakewise](/networks/stakewise/)

[<img src="/images/stader.png" width="100" height="100" />](/images/stader.png)
## [Stader](/networks/stader/)

[<img src="/images/hopr.png" width="100" height="100" />](/images/hopr.png)
## [HOPR](/networks/hopr/)

---
# **Testnets we are backing:**
___
## [Namada](/networks/namada/)
___
## [Penumbra](/networks/penumbra/)

</center>
