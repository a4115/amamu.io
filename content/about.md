---
title: Amamu
---
---

# What is Amamu?


Amamu is an independent entity which offers staking services and truly believes that blockchain technology will change the entire world for the better. 


Our society has gone through a period in which everything has been centralized. Communication, law, production and finance are in the hands of very few. And with that great power, came the great abuse. Fortunately, we are now entering a new revolution. One that is assured by technology, and that will allow common people to regain decision-making power. Amamu is strongly committed to helping accomplish that.


We have decided to keep our anonymity, because we seek to be judged for our ideas and actions, and thus avoid prejudices that have to do with race, nationality, gender or socio-economic level. Transparency in what we do is all that matters to us. All our efforts will be aimed at strengthening the chain of collaboration and letting you know how and why we make our decisions at every step we take.


We carefully choose the networks of which we become validators. It is essential for us that they are projects with which we share a series of values. Our main ambition is always to help make decentralization shine.


# Our principles


* We believe in technology-based trust systems.


* We strongly advocate for the open-source-software model.


* We want the future to be transparent, reliable and fair. That's why we love peer-to-peer networks.


* We think that blockchain technology is much more than pure financial interest. It’s a matter of freedom.


* We are thrilled to embrace and help develop the new economy. One that is non-custodial.


* We believe it is also important to run nodes on a non-incentivized testnet in order to help improve the network. Someone has to do it.


* We are convinced that without decentralization real change will never occur. Therefore, we must try to avoid the concentration of staking in the hands of a few. But there are other aspects to be considered, as well, such as diversity of location and data centers and software used by the nodes. 






**We invite you to take a look at the [projects](/projects/) we are participating in.**
